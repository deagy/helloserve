FROM ubuntu
MAINTAINER daniel.eagy@gmail.com

RUN apt-get update
RUN apt-get install golang -y
RUN go get gitlab.com/deagy/helloserve
CMD ["echo","Image created"]